// TO CREATE A CHILD TAGE ELEMENT USING THE JAVA SCRIPT  

//  here the queryselector is common for all selector eg: querySelector('# id_name','.Class_name')
let button = document.querySelector('#btn')

let result = document.createElement('h3')
result.id="output";
document.getElementById('parent').appendChild(result)
button.addEventListener('click',display)

function display(){
 let input= document.querySelector('#input')
 // here the dropdown elements options are in the stored in an array .so we are getting the selected element using the its index "input.selectedIndex" .for only get the value use   ".value"
 let city=input.options[input.selectedIndex].value
 console.log("Your city is : "+city)

 let population=0;language='',growth_rate=0;

 switch(city){
    case 'Bengaluru':population=136080000
                    language='English'
                    growth_rate=124
                    break;
    case 'chennai':population=146080000
                    language='Tamil'
                    growth_rate=125
                    break;
    case 'mumbai':population=156080000
                    language='Hindi'
                    growth_rate=126
                    break;
 }
// TEMPLATE LITERALS ()2015

// * used inside the   ``
// * get the values using ${population,  }

 let text=`The current ${population} of ${city} is $population as of today, based on worldometer elaboration of the latest united Nations data. Growing rate is ${growth_rate} People who spoken language is ${language}.`;;

 document.querySelector('#output').innerHTML=text

}